package edu.uprm.cse.datastructures.cardealer.util;

import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class CircularSortedDoublyLinkedList<E> implements SortedList<E> {
	private Node<E> header;
	private int currentSize;
	private Comparator<E> comp;
	
	public CircularSortedDoublyLinkedList() {
		this.header = new Node<E>();
		this.currentSize = 0;
	}
	
	public CircularSortedDoublyLinkedList(Comparator<E> comp) {
		this.header = new Node<E>();
		this.currentSize = 0;
		this.comp = comp;
		
	}
	

	@Override
	public Iterator<E> iterator() {
		return new CircularDoublyLinkedListIterator<E>();
	}

	@Override
	public boolean add(E obj) {
		if (this.isEmpty()) {
			Node<E> nodeToAdd = new Node<E>(obj,header,header);
			this.header.setNext(nodeToAdd); this.header.setPrev(nodeToAdd);
			this.currentSize++;
			return true;
		}
		else if (this.comp != null){
			Node<E> temp = header.getNext();
			int counter = 0;
			while (counter < this.currentSize+1) {
				if(this.comp.compare(obj, temp.getElement()) < 0) {
					Node<E> nodeToAdd = new Node<E>(obj,temp,temp.getPrev());
					temp.getPrev().setNext(nodeToAdd);temp.setPrev(nodeToAdd);
					currentSize++;
					return true;
				}
				else if(temp.getNext() == header) {
					Node<E> nodeToAdd = new Node<E>(obj,header,header.getPrev());
					header.getPrev().setNext(nodeToAdd); header.setPrev(nodeToAdd);
					this.currentSize++;
					return true;
				}
				temp = temp.getNext();
				counter++;
			}
		}
		else {
			Node<E> nodeToAdd = new Node<E>(obj,header,header.getPrev());
			header.getPrev().setNext(nodeToAdd); header.setPrev(nodeToAdd);
			this.currentSize++;
			return true;
		}
		return false;
	}

	@Override
	public int size() {
		return currentSize;
	}

	@Override
	public boolean remove(E obj) {
		if (this.isEmpty())
			throw new NullPointerException();
		
		if (!this.contains(obj))
			return false;
		
		Node<E> nodeToRemove = this.header.getNext();
		Node<E> nodePrev;
		Node<E> nodeNext;
		
		while(nodeToRemove != header) {
			if (nodeToRemove.getElement().equals(obj)) {
				break;
			}
			nodeToRemove = nodeToRemove.getNext();
		}
		
		nodePrev = nodeToRemove.getPrev();
		nodeNext = nodeToRemove.getNext();
		nodePrev.setNext(nodeNext);
		nodeNext.setPrev(nodePrev);
		nodeToRemove.setNext(null);
		nodeToRemove.setPrev(null);
		nodeToRemove.setElement(null);
		this.currentSize--;
			
		return true;
	}

	@Override
	public boolean remove(int index) {
		if (this.isEmpty())
			throw new NullPointerException();
		
		if (index < 0 || index > this.currentSize)
			throw new IndexOutOfBoundsException();
		
		E i = this.get(index);
		
		return this.remove(i);
	}

	@Override
	public int removeAll(E obj) {
		int counter = 0;
		
		while(this.contains(obj)) {
			this.remove(obj);
			counter++;
		}
		return counter;
	}

	@Override
	public E first() {
		return this.header.getNext().getElement();
	}

	@Override
	public E last() {
		return this.header.getPrev().getElement();
	}

	@Override
	public E get(int index) {
		if (index<0 || index>this.currentSize)
			throw new IndexOutOfBoundsException();
		
		int counter  = 0;
		Node<E> temp = header.getNext();
		while(counter<index) {
			temp = temp.getNext();
			counter++;
		}
		return temp.getElement();
	}

	@Override
	public void clear() {
		while(!this.isEmpty()) {
			this.remove(0);
		}
	}

	@Override
	public boolean contains(E e) {
		return this.firstIndex(e) != -1;
	}

	@Override
	public boolean isEmpty() {
		return this.currentSize == 0;
	}

	@Override
	public int firstIndex(E e) {
		int counter = 0;
		Node<E> temp = header.getNext();
		while(counter< this.currentSize) {
			if(temp.getElement().equals(e)) {
				return counter;
			}
			counter++;
			temp = temp.getNext();
		}
		return -1;
	}

	@Override
	public int lastIndex(E e) {
		int counter = currentSize-1;
		Node<E> temp = header.getPrev();
		while(counter > 0) {
			if(temp.getElement().equals(e)) {
				return counter;
			}
			temp = temp.getPrev();
			counter--;
		}
		return -1;
	}
	
	private static class Node<E> {
		private E element;
		private Node<E> next;
		private Node<E> previous;
		
		public Node(){
			this.element = null;
			this.next = null;
			this.previous = null;
		}
		
		public Node(E element, Node<E> next, Node<E> previous) {
			this.element = element;
			this.next = next;
			this.previous = previous;
		}
		
		public E getElement() {
			return this.element;
		}
		
		public void setElement(E element) {
			this.element = element;
		}
		
		public Node<E> getNext() {
			return this.next;
		}
		
		public void setNext(Node<E> next) {
			this.next = next;
		}
		
		public Node<E> getPrev() {
			return this.previous;
		}
		
		public void setPrev(Node<E> previous) {
			this.previous = previous;
		}
	}
	
	private class CircularDoublyLinkedListIterator<E> implements Iterator<E>{
		private Node<E> nextNode;
		
		public CircularDoublyLinkedListIterator() {
			this.nextNode = (Node<E>) header.getNext();
		}

		@Override
		public boolean hasNext() {
			return nextNode.getElement() != null;
		}

		@Override
		public E next() {
			if (this.hasNext()) {
				E result = this.nextNode.getElement();
				this.nextNode = this.nextNode.getNext();
				return result;
			}
			else {
				throw new NoSuchElementException();
			}
		}
		
	}

}

package edu.uprm.cse.datastructures.cardealer.util;

import edu.uprm.cse.datastructures.cardealer.model.Car;
import edu.uprm.cse.datastructures.cardealer.model.CarComparator;

public class ListTester {

	public static void main(String[] args) {
		CircularSortedDoublyLinkedList<Car> list = new CircularSortedDoublyLinkedList<Car>(new CarComparator());
		
		list.add(new Car(1,"toyota", "camry", "LE", 20000));
		list.add(new Car(2,"toyota", "camry", "SE", 20000));
		list.add(new Car(3,"toyota", "corolla", "SE", 12000));
		list.add(new Car(4,"honda", "odassey", "E", 15000));
		list.add(new Car(5,"lamborgini", "huracan", "XP", 500000));
		list.add(new Car(6,"nissan", "GT-R", "nissmo", 152000));



		for(int i = 0; i<list.size();i++) {
			System.out.println(list.get(i).toString());
		}
	}
}
